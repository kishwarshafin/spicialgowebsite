<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
//		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function home()
	{
		return view('pages.home');
	}

    public function fileStore(Request $request)
    {

        if($request->file('file'))
        {
           if($request->file('file')->getClientOriginalExtension()=="txt") {


               $ext='';

               $filename = base_path().'/public/out.txt';

               if (File::exists($filename)) {
                   File::delete($filename);
                   $ext="Delete ";
               }

               $imageName = 'in.' . $request->file('file')->getClientOriginalExtension();
               $success=$request->file('file')->move(base_path() . '/public/', $imageName);

               $codeToRun = $request->input('codeToRun');

                if($success) {
                    if ($codeToRun == 1)
                        $outputCom = `./runSpici+1.sh`;
                    else if ($codeToRun == 2)
                        $outputCom = `./runSpici+2.sh`;
                    else if ($codeToRun == 3)
                        $outputCom = `./runSpici+12.sh`;


                    return redirect('/home')->with('message', $ext . ' ' . $outputCom );
                }

           }
        }

        return redirect('/home')->withErrors('Insufficient Permission');
    }

    public function getDownload(){
        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/out.txt";

        $headers = array(
            'Content-Type: application/octet-stream'
        );
        return Response::download($file, 'output.txt', $headers);


    }
}
