<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


get('/home', 'HomeController@home');
post('/home',['as'=>'home','uses'=>'HomeController@fileStore'] );

get('/download', 'HomeController@getDownload');