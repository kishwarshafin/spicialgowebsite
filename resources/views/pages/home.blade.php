<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Home</title>

    <meta http-equiv="Content-Type" content="text/html;
	           charset=UTF-8" />

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>



</head>


<body>



<!-- page content starts here -->
<div class="container">
    <div class="row">
        <div class="text-center">
            <br><br>
           <h1><font size="7" face="Georgia, Arial" color="maroon">SPICi</font>:&nbsp&nbsp <font size="7" face="Georgia, Arial" color="maroon">S</font>peed and <font size="7" face="Georgia, Arial" color="maroon">P</font>erformance <font size="7" face="Georgia, Arial" color="maroon">I</font>n <font size="7" face="Georgia, Arial" color="maroon">C</font>luster<font size="7" face="Georgia, Arial" color="maroon">i</font>ng</h1>
           <br>
        </div>
        <div class="text-center">
           <h4> <b>SPICi</b> (pronounced "spicy") is a fast local network clustering algorithm.
            SPICi runs in time O(Vlog V +E) and space O(E), where V and E are the number of vertices and edges in the network.
            It also has state-of the-art performance with respect to the quality of the clusters it uncovers.
            The source code of SPICi could be found in Downloads section; or you could try it online with the form below.
            The help content of each field will be displayed upon mouse over.
           </h4>
        <br>
        <br>
        <br>
        </div>
    <div>
    <div class="row" style="border: 2px solid #23527c">
        <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 text-center" >

            <br><br>

            {!! Form::open(array('url'=>'/home','method'=>'POST', 'files'=>true)) !!}

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <h4>Upload Your Network</h4><br>
            <div class="form-group col-sm-3 col-sm-offset-3 col-md-3 col-md-offset-3 col-lg-3 col-lg-offset-3">
                <input type="file"  name="file" accept="text/*"  required="">
            </div>
            <br>
            <br>
            <h4>
                Code To Run
                <br>
                <br>
                <select name="codeToRun">
                    <option value="1">Code 1</option>
                    <option value="2">Code 2</option>
                    <option value="3">Code 3</option>
                </select>
            </h4>
            <br>
            <br>
            <div class="form-group ">
                <input type="submit" name='publish' class="btn btn-success" value = "Upload File & Run"/>
            </div>
            <br>
            {!! Form::close() !!}

            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}<br>
                </div>

                <div>
                    <a href="/download"> </i> Download Output </a>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                </div>
            @endif
        </div>




        <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center">
                <br>
               <h4> <b>Input</b>: Each line represents one edge in the format of <b>ID_A&nbsp ID_B&nbsp weight</b>, separated by space or tab.<br>
                Example:<br>
                A&nbsp B&nbsp 0.7<br>
                C&nbsp D&nbsp 0.9<br>
                ...<br>
                <b>Output</b>: Each line represents a cluster. Gene names are separated by tab.<br>
            <br><h4>
        </div>

    </div>
</div>



</body>

</html>